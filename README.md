# vs-test

### Demo
[```https://videoslots.ali-murad.com/```](https://videoslots.ali-murad.com/)

### Project setup
```npm install```

### Compiles and hot-reloads for development
```npm run serve```

### Compiles and minifies for production
```npm run build```

### Highlights / Extra points

1. Debounce is implemented of 500ms for filter and page change
2. Clicking on image opens modal box which also includes option to open ```url``` in new tab.
3. Clicking on magnifying glass icon in the top bar activates filter.
4. SCSS was used for styling, including implementation of mixin for box shadow.

### Design justification

Design inspiration came from multiple platforms. Mainly the idea was to keep the design to the minimal while writing the entire styles and not using any external desinged component. Notification and button design inspiration came from Material UI. 

### What would I have done differently if I had more time?

1. Add test cases.
2. Create a small backend service to mask the API requests.


