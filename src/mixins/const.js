export default {
    baseUrl: 'https://picsum.photos',
    urlVersion: 'v2',
    labels: {
        prev: 'Prev',
        next: 'Next',
        page: 'Page',
        itemsPerPage: 'Images per page',
        searchPlaceHolder: 'Filter by author...',
        brandName: 'Image Library'
    },
    defaultImageLimit: 30,
    imageWidth: 367,
    imageHeight: 267,
    proxyUrl: 'https://jsonp.afeld.me/?url='
};