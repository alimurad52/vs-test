import Vue from 'vue'
import App from './App.vue'
import config from './mixins/const';

Vue.config.productionTip = false

Vue.mixin({
  data: () => config
});

new Vue({
  render: h => h(App),
}).$mount('#app')
